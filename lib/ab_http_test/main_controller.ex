defmodule AbHttpTest.MainController do
  require Logger
  import AbHttp.Connection

  def index(conn, _opts) do
    conn
    |> append_response("""
    <h1>IT WORKS</h1>
    <form method="post">
      <input name="name">
      <input type="submit">
    </form>
    <script>
      var ws = new WebSocket("ws://localhost:8000/echo");
      ws.addEventListener("message", (msg) => console.log(msg));
      ws.addEventListener("close", () => console.log("<closed>"));
      ws.addEventListener("open", () => ws.send("Hello!"));
    </script>
    """)
  end

  def greet(conn, %{"name" => name}) do
    conn
    |> append_response("<p>Hello, #{name}</p>")
  end

  def user_settings(conn, %{"name" => name}) do
    conn
    |> append_response("<h1>User settings for user <b>#{name}</b></h1>")
  end

  def cookie_test(conn, _opts) do
    case conn.cookies["test"] do
      nil -> append_response(conn, "<p>No test cookie</p>")
      text -> append_response(conn, "<p>Cookie has value <b>#{text}</b></p>")
    end
    |> append_response("""
      <form method="post" action="#">
        <input name="test-cookie">
        <input type="submit">
      </form>
    """)
  end

  def set_test_cookie(conn, %{"test-cookie" => value}) do
    exp_date = DateTime.utc_now()
    |> DateTime.add(7, :day)

    conn
    |> set_cookie("test", value, expires: exp_date, same_site: :strict)
    |> add_header("Location", "/cookie-test")
    |> put_status(302)
  end
end
