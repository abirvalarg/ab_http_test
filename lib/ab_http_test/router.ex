defmodule AbHttpTest.Router do
  use AbHttp.Router
  alias AbHttpTest.EchoWebSocket
  alias AbHttp.Connection
  alias AbHttpTest.{Secret, MainController}
  require Logger

  get "/", MainController, :index
  post "/", MainController, :greet

  get "/greet/:name", MainController, :greet
  get "/user/:name/settings", MainController, :user_settings

  get "/cookie-test", MainController, :cookie_test
  post "/cookie-test", MainController, :set_test_cookie

  # Don't do this, it's here just for demonstration
  scope "/secret/:name", Secret.Router
  scope "/secret", Secret.Router

  websocket "/echo", EchoWebSocket

  after_routing do
    plug :log
    plug :content_type

    def log(%Connection{} = conn, _opts) do
      Logger.info("#{conn.method} #{conn.path} #{conn.resp_status}")
      conn
    end

    def content_type(%Connection{} = conn, _opts) do
      if Connection.has_response_header(conn, "content-type") do
        conn
      else
        Connection.add_header(conn, "Content-Type", "text/html")
      end
    end
  end
end
