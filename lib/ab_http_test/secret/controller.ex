defmodule AbHttpTest.Secret.Controller do
  import AbHttp.Connection

  def index(conn, %{"name" => name}) do
    conn
    |> append_response("Welcome to the secret zone for user <b>#{name}</b>")
  end

  def index(conn, _opts) do
    conn
    |> append_response("Welcome to the secret zone")
  end
end
