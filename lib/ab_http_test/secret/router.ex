defmodule AbHttpTest.Secret.Router do
  use AbHttp.Router
  require Logger
  alias AbHttpTest.Secret.Controller

  plug :log_secret

  get "/", Controller, :index

  def log_secret(conn, _opts) do
    Logger.info("Someone found a secret zone")
    conn
  end
end
