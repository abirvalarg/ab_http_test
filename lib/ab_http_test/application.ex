defmodule AbHttpTest.Application do
  use Application

  @impl Application
  def start(_start_type, _start_args) do
    children = [
      AbHttp.HandlerSupervisor,
      {AbHttp.Server.Tcp, endpoint: AbHttpTest.Router}
    ]

    Supervisor.start_link(children, strategy: :rest_for_one)
  end
end
