defmodule AbHttpTest.EchoWebSocket do
  use AbHttp.WebSocket
  require Logger

  @impl AbHttp.WebSocket
  def init(_conn, _opts) do
    {:ok, nil}
  end

  @impl AbHttp.WebSocket
  def handle_frame({:ping, data}, state), do: {:push, {:pong, data}, state}
  def handle_frame(frame, state) do
    Logger.debug(inspect(frame))
    {:push, frame, state}
  end
end
