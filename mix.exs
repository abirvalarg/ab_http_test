defmodule AbHttpTest.MixProject do
  use Mix.Project

  def project do
    [
      app: :ab_http_test,
      version: "0.1.0",
      elixir: "~> 1.14",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {AbHttpTest.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:ab_http, "~> 0.2.1", git: "https://gitlab.com/abirvalarg/ab_http"}
      # {:ab_http, path: "../ab_http"}
    ]
  end
end
